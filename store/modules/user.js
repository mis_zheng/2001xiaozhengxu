import {
	login
} from "../../api/modules/my.js"
const user = {
	namespaced: true, //开启命名空间
	state: () => ({
		token: uni.getStorageSync('token') || "", //数据持久化和本地存储
		userinfo: uni.getStorageSync('userinfo') || {}
	}),
	mutations: {
		// 接收异步信息
		chnaguseinfo(state, payload) {
			// console.log(payload,'payload')
			state.token = payload.code
			state.userinfo = payload.userinfo
			// 当前模块下的同步调用 user表示当前模块
			this.commit('user/mutationslocalStorage')
		},
		// 同步调用方法---存储数据
		mutationslocalStorage(state) {
			uni.setStorageSync('token', state.token)
			uni.setStorageSync('userinfo', state.userinfo)
		},
		// 退出登录
		clearlogininfo(state) {
			state.token = ''
			state.userinfo = {}
			uni.setStorageSync('token', state.token)
			uni.setStorageSync('userinfo', state.userinfo)
		}
	},
	actions: {
		// async	getToken(context,payload){//下面是解构所需要数据
		// 获取登录信息和token值
		async getToken({
			state,
			commit
		}, {
			code,
			userinfo
		}) {
			// console.log(context,'context')
			// console.log(payload,'payload')
			// 获取登录信息
			let res = await login({
				principal: code
			})
			// console.log(res)
			commit('chnaguseinfo', {
				code: res.access_token,
				userinfo: userinfo
			})
		},
		// 判断是否登录
		async isLogin({
			state
		}) {
			// 判断有没有登录
			if (state.token) return true
			// 没有登录就跳转登录
		const [error,res]=	await uni.showModal({
				title: "登录",
				content: "当前没有登录,请先登录"
			})
			console.log(res.confirm)
			// 确定要登录,跳转登录页
			if(res.confirm){
				uni.navigateTo({
					url:`/login/logincode/logincode`
				})
			}
		}
	}
}
export default user
