import httpRequest from "../../utils/httprequest.js";
//首页轮播
export const indexImg=(params)=>httpRequest.get("/indexImgs",params);

//首页热卖商城
export const prodListByTag=(params)=>httpRequest.get("/prod/prodListByTagId",params);
//通知
export const topNoticeList=(params)=>httpRequest.get("/shop/notice/topNoticeList",params);
// https://bjwz.bwie.com/mall4j/prod/prodInfo?prodId=70
// 详情页
export const prodInfo=(params)=>httpRequest.get("/prod/prodInfo?",params);
