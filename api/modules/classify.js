import httpRequest from "../../utils/httprequest.js";

//分页侧边栏数据及图片
export const category=(params)=>httpRequest.get("/category/categoryInfo?parentId=",params);

// 分页右侧数据图片
export const prod=(params)=>httpRequest.get("/prod/pageProd?categoryId=",params);