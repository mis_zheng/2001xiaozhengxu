import httpRequest from "../../utils/httprequest.js";

//热门搜索search/hotSearchByShopId?number=10&shopId=1&sort=1
export const hotSearchByShopId=(params)=>httpRequest.get("/search/hotSearchByShopId",params);
//搜索结果 search/searchProdPage
export const searchProdPage=(params)=>httpRequest.get("/search/searchProdPage",params);