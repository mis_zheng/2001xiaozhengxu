import {base_url} from "./config.js"
class HttpRequest{
	request(url,method,params){	//相当于es5 HttpRequest.prototype.request=function()
		uni.showLoading({
			title:"正在加载中..."
		})
		return new Promise((reslove,reject)=>{
			uni.request({
				url:base_url+url,
				data:params,
				method,
				success: (res) => {
					if(res.statusCode==200)
					{
						reslove(res.data)
						uni.hideLoading()
					}
				},
				fail: (err) => {
					reject(err);
					uni.hideLoading()
					uni.showToast({
						title:"请求失败",
						duration:2000
					})
				},
				complete: () => {//不管失败还是成功都会执行
					uni.hideLoading()
				}
			})
		})
	};
	get(url,params){
		console.log(url,params,"params")
		return this.request(url,"GET",params)
	};
	post(url,params){
		 return this.request(url,'POST',params)
	}
}
 
export default new HttpRequest()
