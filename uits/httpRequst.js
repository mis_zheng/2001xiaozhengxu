import { base_url } from "./config.js";
class HttpRequest {
  request(url, method, params) {
    //相当原型上的方法
    uni.showLoading({
      //数据加载提示
      title: "加载中",
    });
    return new Promise((resolve, reject) => {
      uni.request({
        url: base_url + url, //仅为示例，并非真实接口地址。
        data: params,
        method,
        success: (res) => {
          if (res.statusCode == 200) {
            resolve(res.data);
            uni.hideLoading(); //关闭加载提示
          }
        },
        fail: (err) => {
          reject(err);
          uni.hideLoading();
          uni.showToast({
            //请求失败提示
            title: "请求失败提示",
            duration: 2000,
          });
        },
        complete: () => {
          //不管成功，失败都执行
          uni.hideLoading();
        },
      });
    });
  }
  get(url, params = {}) {
    return this.request(url, "GET", params);
  }
  post(url, params = {}) {
    return this.request(url, "POST", params);
  }
}
export default new HttpRequest();
